package redbull.contentviewer

import org.bson.types.ObjectId
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.*
import redbull.contentviewer.db.ContentRepository

import org.springframework.validation.BindingResult

import org.springframework.web.bind.annotation.ModelAttribute

import redbull.contentviewer.db.Review
import java.awt.print.Pageable


@Controller
class ContentViewerController( val contentRepository: ContentRepository ) {

    @GetMapping("/")
    fun browse(model: Model): String {
        model["title"] = "Browse"
        model["content"] = contentRepository.findAll()
        return "contentviewer"
    }

    @GetMapping("/browse")
    fun browse(@RequestParam search: String, model: Model): String {
        model["title"] = "Browse"
        model["searchTerm"] = search;
        model["content"] = contentRepository.findByTitleLikeOrTopicLikeOrDescriptionLikeQuery(search)
        return "contentviewer"
    }

    @GetMapping("/videos")
    fun videos(model: Model): String {
        model["title"] = "Browse videos"
        model["content"] = contentRepository.findAllByMediaType("video")
        return "contentviewer"
    }

    @GetMapping("/images")
    fun images(model: Model): String {
        model["title"] = "Browse images"
        model["content"] = contentRepository.findAllByMediaType("image")
        return "contentviewer"
    }

    @GetMapping("/image/{id}")
    fun imageDetail(@PathVariable id: String, model: Model): String {
        contentRepository.findById(ObjectId(id)).let{ image -> model["content"] = image.get() }
        return "detailview"
    }

    @PostMapping("/image/{id}")
    fun imageReview(@PathVariable id: String, @ModelAttribute("review") review: Review, result: BindingResult, model: Model): String {
        if (result.hasErrors()) {
            return "error";
        }

        contentRepository.findById(ObjectId(id)).let{ image ->
            image.get().addReview( review );
            contentRepository.save( image.get() );
            model["content"] = image.get()
        }
        return "detailview"
    }

    @GetMapping("/video/{id}")
    fun videoDetail(@PathVariable id: String, model: Model): String {
        contentRepository.findById(ObjectId(id)).let{ video -> model["content"] = video.get() }
        return "detailview"
    }

    @PostMapping("/video/{id}")
    fun videoReview(@PathVariable id: String, @ModelAttribute("review") review: Review, model: Model): String {
        contentRepository.findById(ObjectId(id)).let{ video ->
            video.get().addReview( review );
            contentRepository.save( video.get() );
            model["content"] = video.get()
        }
        return "detailview"
    }

    @GetMapping("/topic/{topicName}")
    fun topic(@PathVariable topicName: String, model: Model): String {
        model["title"] = "Browse topic"
        model["searchTerm"] = topicName;
        model["content"] = contentRepository.findAllByTopic(topicName)
        return "contentviewer"
    }

    @GetMapping("/top")
    fun topic(model: Model): String {
        model["title"] = "Top 10"
        model["content"] = contentRepository.findAll(PageRequest.of(0,10, Sort.by(Sort.Direction.DESC, "averageRating")));
        return "contentviewer"
    }
}