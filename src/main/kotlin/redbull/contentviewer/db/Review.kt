package redbull.contentviewer.db

data class Review(
    val name: String,
    val text: String,
    val rating: Int = 0
)
