package redbull.contentviewer.db

import org.bson.types.ObjectId
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query

interface ContentRepository : MongoRepository<Content, ObjectId> {
    fun findAllByMediaType(mediaType: String): Iterable<Content>
    fun findAllByTopic(topicName: String): Iterable<Content>

    @Query("{'\$or': [{'title':       { \$regex: /.*?0.*/, \$options: 'i'}}," +
                     "{'topic':       { \$regex: /.*?0.*/, \$options: 'i'}}," +
                     "{'description': { \$regex: /.*?0.*/, \$options: 'i'}}]}")
    fun findByTitleLikeOrTopicLikeOrDescriptionLikeQuery(likeText: String): Iterable<Content>
    override fun deleteAll()
}