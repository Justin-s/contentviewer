package redbull.contentviewer.db

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.ReadOnlyProperty
import org.springframework.data.mongodb.core.mapping.Document

@Document( collection = "content" )
data class Content (
    @Id
    val _id: ObjectId = ObjectId.get(),
    val mediaType: String?,
    val source: String?,
    val title: String?,
    val description: String?,
    val length: String?,
    val aspectRatio: String?,
    val topic: String?,
    val contentUrl: String?,
    val previewUrl: String?,
    var averageRating: Double?,
    val reviews: MutableList<Review> = mutableListOf(),

    @ReadOnlyProperty
    val isImage: Boolean = mediaType == "image",
    @ReadOnlyProperty
    val isVideo: Boolean = mediaType == "video"
){
    fun addReview( review: Review ){
        reviews.add( review )
        averageRating = reviews.map { it.rating }.average()
    }
}