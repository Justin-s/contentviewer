package redbull.contentviewer

import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import com.samskivert.mustache.Mustache
import com.samskivert.mustache.Mustache.TemplateLoader

import org.springframework.boot.autoconfigure.mustache.MustacheEnvironmentCollector
import org.springframework.context.annotation.Bean
import org.springframework.core.env.Environment


@SpringBootApplication
class ContentViewerApplication{

	//Override MustacheAutoConfiguration to support defaultValue("")
	@Bean
	fun mustacheCompiler( mustacheTemplateLoader: TemplateLoader, environment: Environment ): Mustache.Compiler {
		val collector = MustacheEnvironmentCollector()
		collector.setEnvironment(environment)

		// default value
		return Mustache.compiler().defaultValue("")
			.withLoader(mustacheTemplateLoader)
			.withCollector(collector)
	}
}

fun main(args: Array<String>) {
	runApplication<ContentViewerApplication>(*args) {
		setBannerMode(Banner.Mode.OFF)
	}
}
